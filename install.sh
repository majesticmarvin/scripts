#! /bin/bash
# Install wget and other utils to complete install
sudo apt update && sudo apt install wget git -y &&

# Install VSCodium
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg &&

echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' \
    | sudo tee /etc/apt/sources.list.d/vscodium.list &&

sudo apt update &&
sudo apt install codium -y &&

# Install Geany and plugins
sudo apt update && sudo apt install geany geany-plugin-markdown -y &&

# Install and setup NVM/NodeJS(LTS)
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash &&
echo "NVM now installed please run nvm install --lts to install NodeJS(LTS)"

# Install and setup ZSH/Oh-My-Zsh
sudo apt update && sudo apt install zsh -y &&
zsh --version &&
chsh -s $(which zsh) 
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
